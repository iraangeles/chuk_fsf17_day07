(function() {

    var LoginApp = angular.module("LoginApp", []);

    var LoginCtrl = function($http, $log) {
        var loginCtrl = this;
        
        loginCtrl.username = "";
        loginCtrl.password = "";

        loginCtrl.performLogin = function() {
            $log.info("username: %s, password: %s", loginCtrl.username, loginCtrl.password);
            $http.get("/login", {
                params: {
                    username: loginCtrl.username,
                    password: loginCtrl.password,
                    timestamp: (new Date()).toString()
                }
            
            }).then(function(result) {
                console.log("Login successfull");
                loginCtrl.status = "Login is successful: token = " + JSON.stringify(result.data);

            }).catch(function(status) {
                loginCtrl.status = "Username or password are incorrect. Try again"
            
            })
        }
    }

    //Strict Dependency Injection
    LoginApp.controller("LoginCtrl", [ "$http", "$log", LoginCtrl ]);

})();