//Load libraries
var path = require("path");
var express = require("express");

//Create an instance of the app
var app = express();

//Configure routes
app.use(express.static(path.join(__dirname, "public")));

app.use("/libs", express.static(path.join(__dirname, "bower_components")))

app.get("/login", function(req, resp) {

	console.log("username: %s, password: %s", req.query.username, req.query.password);

    //BEWARE: this is very difficult to understand and difficult to debug cause you can't see it 
	//that well
	var status = (req.query.password == req.query.username)? 202: 400;
	resp.status(status);
	if (202 == status) {
		//Access the database 
		resp.type("application/json");
		resp.json({
			token: Math.random()
		});
	} 
	resp.end();

});

//Configure server
app.set("port", 3000);

app.listen(app.get("port"), function() {
	console.log("Application started on port %d", app.get("port"));
});
